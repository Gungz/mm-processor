const _ = require("lodash");
const config = require("config");
const uuid = require("uuid/v4");
const helper = require("../common/helper");
const logger = require("../common/logger");

/**
 * Creates a new review record based on the score from Scorer
 */
class ReviewProducerService {
  /**
   * Constructs new review producer service.
   * @param {Object} config Takes config object with the following properties:
   * - SUBMISSION_API_URL - The submission API endpoint
   * - REVIEW_TYPE_NAME - The name of the review type to use, e.g. "TCO2018-F2FScorer"
   * - REVIEW_SCORECARD_ID: The review scorecard id to use, e.g. 30001850
   * - REVIEWER_ID_NAMESPACE: The GUID defining namespace for generating reviewerId as GUID based on the 'sub' value from JWT
   */
  constructor(config) {
    this.config = config;
  }

  /**
   * Calls the 'generateReview' method of the API.
   * @param {String} token M2M token value
   * @param {String} submissionId Submission Id, GUID
   * @param {String} reviewerId Reviewer Id, GUID
   * @param {String} reviewTypeId Review type Id, GUID
   * @param {Number} score Score value in percent from 0 to 100
   * @param {Object} testPhase Test meta-data
   * @returns Response object returned from 'createReview' API
   */
  async generateReview(
    token,
    submissionId,
    reviewerId,
    reviewTypeId,
    score,
    testPhase
  ) {
    let metadata = {};
    if (testPhase === "system") {
      metadata.testType = testPhase;
      return (await helper
        .getApi(token)
        .post("/reviewSummations")
        .send({
          submissionId,
          scoreCardId: this.config.REVIEW_SCORECARD_ID,
          aggregateScore: score,
          metadata: metadata,
          isPassing: true
        })).body;
    } else {
      metadata.testType = "provisional";
      return (await helper
        .getApi(token)
        .post("/reviews")
        .send({
          submissionId,
          scoreCardId: this.config.REVIEW_SCORECARD_ID,
          reviewerId,
          metadata: metadata,
          typeId: reviewTypeId,
          score
        })).body;
    }
  }

  /**
   * Main function of the service. Creates review from the score provided by Scorer
   * and creates new review record.
   * @param {String} submissionId Submission Id, GUID
   * @param {Number} score Score from the scorer
   * @param {Object} metadata Metadata if any from the scorer in case of errors
   */
  async createReview(submissionId, score, metadata) {
    const token = await helper.getM2Mtoken();
    const reviewTypeId = await helper.getReviewTypeId(
      this.config.REVIEW_TYPE_NAME
    );
    const reviewerId = uuid();
    await this.generateReview(
      token,
      submissionId,
      reviewerId,
      reviewTypeId,
      score,
      metadata
    );
    logger.info(
      `Created review for submission ${submissionId} with score ${score}`
    );
  }
}

module.exports = ReviewProducerService;
