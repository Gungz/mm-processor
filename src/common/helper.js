/**
 * Contains generic helper methods
 */
const _ = require("lodash");
const http = require("http");
const request = require("superagent");
const prefix = require("superagent-prefix");
const unzip = require("unzipper");
const path = require("path");
const config = require("config");
const m2mAuth = require("tc-core-library-js").auth.m2m;
const m2m = m2mAuth(
  _.pick(config, ["AUTH0_URL", "AUTH0_AUDIENCE", "TOKEN_CACHE_TIME"])
);
const logger = require("./logger");
const childProcess = require("child_process");
const streamifier = require("streamifier");

// Variable to cache reviewTypes from Submission API
const reviewTypes = {};

/**
 * Function to get M2M token
 * @returns {Promise}
 */
async function getM2Mtoken() {
  return m2m.getMachineToken(
    config.AUTH0_CLIENT_ID,
    config.AUTH0_CLIENT_SECRET
  );
}

/**
 * Attempts to download a zip file and unzip it
 * @param {String} submissionId Submissio ID
 * @returns {Promise}
 */
async function downloadAndUnzipFile(submissionId) {
  const subPath = path.join(__dirname, "../../submissions", submissionId);

  logger.info(`Downloading submission ${submissionId}`);
  const url = `${
    config.SUBMISSION_API_URL
  }/submissions/${submissionId}/download`;
  const zipFile = await reqSubmission(url);

  await streamifier
    .createReadStream(zipFile.body)
    .pipe(
      unzip.Extract({
        path: `${subPath}/submission`
      })
    )
    .promise();

  logger.info(`Zip file extracted to ${subPath}/submission`);
  return subPath;
}

/**
 * Helper function returning prepared superagent instance.
 * @param {String} token M2M token value
 * @returns {Object} superagent instance configured with Authorization header and API url prefix
 */
function getApi(token) {
  return request
    .agent()
    .use(prefix(config.SUBMISSION_API_URL))
    .set("Authorization", `Bearer ${token}`);
}

/**
 * Function to get reviewTypeId from name
 * @param {String} reviewTypeName Name of the reviewType
 * @returns {String} reviewTypeId
 */
async function getReviewTypeId(reviewTypeName) {
  const token = await getM2Mtoken();
  if (reviewTypes[reviewTypeName]) {
    return reviewTypes[reviewTypeName];
  } else {
    const response = await getApi(token)
      .get("/reviewTypes")
      .query({
        name: reviewTypeName
      });
    if (response.body.length !== 0) {
      reviewTypes[reviewTypeName] = response.body[0].id;
      return reviewTypes[reviewTypeName];
    }
    return null;
  }
}

/**
 * Helper function returning prepared superagent instance for using with challenge API.
 * @param {String} token M2M token value
 * @returns {Object} superagent instance configured with Authorization header and API url prefix
 */
function getV3Api(token) {
  return request
    .agent()
    .use(prefix(config.CHALLENGE_API_URL))
    .set("Authorization", `Bearer ${token}`);
}

/**
 * Function to get challenge description by its id
 * @param {String} challengeId challenge id
 * @returns {Object} challenge description
 */
async function getChallenge(challengeId) {
  const token = await getM2Mtoken();
  const response = await getV3Api(token).get(`/challenges/${challengeId}`);
  const content = _.get(response.body, "result.content");
  if (content) {
    return content;
  }
  return null;
}

/**
 * Function to send GET request to Submission API
 * @param {String} url Complete Submission API URL
 * @returns {Object} Submission information
 */
async function reqSubmission(url) {
  const token = await getM2Mtoken();
  return getApi(token)
    .get(url)
    .maxResponseSize(524288000);
}

/*
 * Post Error to Bus API
 * {Object} error Error object
 */
async function postError(error, submissionDetails = {}) {
  // Request body for Posting error to Bus API
  const errMessage = error.message ? error.message : error;
  const reqBody = {
    topic: config.KAFKA_ERROR_TOPIC,
    originator: "cmap-scorer",
    timestamp: new Date().toISOString(),
    "mime-type": "application/json",
    payload: {
      error: {
        errorMessage: errMessage,
        submissionDetails
      }
    }
  };
  logger.info(`Post error to Bus API with data: ${JSON.stringify(reqBody)}`);
  const token = await getM2Mtoken();
  return request
    .post(config.BUSAPI_URL)
    .set("Authorization", `Bearer ${token}`)
    .set("Content-Type", "application/json")
    .send(reqBody);
}

/**
 * Takes a kafka message payload and assuming it is a submission
 * gets the submission object
 * @param {Object} submissionId The id of submission
 */
async function getSubmission(submissionId) {
  const url = `${config.SUBMISSION_API_URL}/submissions/${submissionId}`;
  logger.debug(`Getting submission from: ${url}`);

  try {
    const response = await reqSubmission(url);
    return response.body;
  } catch (error) {
    logger.logFullError(error);
    throw error;
  }
}

module.exports = {
  getM2Mtoken,
  downloadAndUnzipFile,
  getApi,
  getReviewTypeId,
  getV3Api,
  getChallenge,
  reqSubmission,
  postError,
  getSubmission
};
