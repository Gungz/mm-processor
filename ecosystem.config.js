module.exports = {
  apps: [
    {
      name: "<APP NAME>",
      script: "src/app.js",
      instances: 1,
      env: {
        KAFKA_URL: "",
        GROUP_CONSUMER_NAME: "<NAME OF GROUP CONSUMER>",
        KAFKA_CLIENT_CERT: "",
        KAFKA_CLIENT_CERT_KEY: "",
        AUTH0_AUDIENCE: "",
        AUTH0_CLIENT_ID: "",
        AUTH0_CLIENT_SECRET: "",
        AUTH0_URL: "",
        SUBMISSION_API_URL: "",
        CHALLENGE_API_URL: "",
        
        REVIEW_TYPE_NAME: "<NAME OF REVIEW TYPE>",
        BUSAPI_URL: "",

        AWS_ACCESS_KEY_ID: "",
        AWS_DEFAULT_REGION: "",
        AWS_REGION: "",
        AWS_SECRET_ACCESS_KEY: "",

        S3_BUCKET: "",
        TESTING_TIMEOUT: 7200000, //Default 2 hours
        CHALLENGE_ID: 00000000,

        TESTER_COMMAND: "",
        SOLUTION_COMMAND: "",
        DOCKER_IMAGE_NAME: "somethingrandom", // Update this when Dockerfile is build and published
        DOCKER_MOUNT_PATH: "",
        NODE_ENV: ""
      }
    }
  ]
};
